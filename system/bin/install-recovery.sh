#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/155a0000.ufs/by-name/RECOVERY:45746176:dcf6442e1cc339181dd4a0c160d6ce00fdd1994f; then
  applypatch EMMC:/dev/block/platform/155a0000.ufs/by-name/BOOT:37576704:710faad693e35f239e939c55bf6cdd57d30e207a EMMC:/dev/block/platform/155a0000.ufs/by-name/RECOVERY dcf6442e1cc339181dd4a0c160d6ce00fdd1994f 45746176 710faad693e35f239e939c55bf6cdd57d30e207a:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
