#!/sbin/sh
# Written by Tkkg1994

mount /dev/block/platform/155a0000.ufs/by-name/USERDATA /data

if [ ! -d /data/media/0/SuperStock ]; then
	mkdir /data/media/0/SuperStock
	chmod 777 /data/media/0/SuperStock
fi

cp -rf /tmp/aroma /data/media/0/SuperStock

find /data/media/0/SuperStock/aroma -type f ! -iname "*.prop" -delete

cp -rf /tmp/aroma/.install.log /data/media/0/SuperStock/aroma/install.log

exit 10

